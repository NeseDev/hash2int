#hash2int

```js
const hash2int = require('hash2int');
let number = hash2int('<hash>', '<max>', '<min>');
```

```js
// get a number between 0 and 15
let number = hash2int('edd983fc373b7230157a7c05916c715b');
// => 2
```
```js
// get a number between 0 and 31
let number = hash2int('edd983fc373b7230157a7c05916c715b', 32);
// => 18
```
```js
// get a number between 5 and 31
let number = hash2int('edd983fc373b7230157a7c05916c715b', 32, 5);
// => 11
```
module.exports =  (hash, max = 16, min = 0) => {
  let range = max - min;
  let intArray = hashToIntArray(hash);
  let sum = intArray.reduce(getSum);
  let num = sum % range;
  let result = num + min;
  return result;
}

const hashToIntArray = (hexString) => new Uint8Array(hexString.match(/.{1,2}/g).map(byte => parseInt(byte, 16)));

const getSum = (total, num) => total + num;